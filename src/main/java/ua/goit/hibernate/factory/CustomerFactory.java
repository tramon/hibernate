package ua.goit.hibernate.factory;

import ua.goit.hibernate.dao.CustomerDao;

import java.sql.SQLException;

/**
 * Created by tramon on 30.11.2016.
 */
public class CustomerFactory {

    public void createCustomer (String name) throws SQLException {
        CustomerDao customerDao = new CustomerDao();
        customerDao.createElement(name);
    }

}
