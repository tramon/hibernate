package ua.goit.hibernate.factory;

import ua.goit.hibernate.dao.ProjectDao;

import java.sql.SQLException;

public class ProjectFactory {

    public void createProject(String name, int cost) throws SQLException {
        ProjectDao projectDao = new ProjectDao();
        projectDao.createElement(name, cost);
    }
}
