package ua.goit.hibernate.factory;

import ua.goit.hibernate.dao.CompanyDao;

import java.sql.SQLException;

/**
 * Created by tramon on 30.11.2016.
 */
public class CompanyFactory {

    public void createCompany (String name, String address) throws SQLException {
        CompanyDao companyDao = new CompanyDao();
        companyDao.createElement(name, address);
    }

}
