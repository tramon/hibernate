package ua.goit.hibernate.factory;


import ua.goit.hibernate.dao.DeveloperDao;

import java.sql.SQLException;

public class DeveloperFactory{

    public void createDeveloper(String name, String surname, int salary) throws SQLException {
        DeveloperDao developerDao = new DeveloperDao();
        developerDao.createElement(name, surname, salary);
    }
}
