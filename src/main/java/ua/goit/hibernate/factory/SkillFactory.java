package ua.goit.hibernate.factory;

import ua.goit.hibernate.dao.SkillDao;

import java.sql.SQLException;

/**
 * Created by tramon on 30.11.2016.
 */
public class SkillFactory {

    public void createSkill(String name, String level) throws SQLException {
        SkillDao skillDao = new SkillDao();
        skillDao.createElement(name, level);
    }
}
