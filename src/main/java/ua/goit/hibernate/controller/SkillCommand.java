package ua.goit.hibernate.controller;


import ua.goit.hibernate.factory.SkillFactory;
import ua.goit.hibernate.view.ConsoleHelper;
import ua.goit.hibernate.dao.SkillDao;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by tramon on 30.11.2016.
 */
public class SkillCommand implements Command {

    private final String SKILL = "Skill";
    private final String SKILLS = "Skills";

    @Override
    public void execute() throws IOException, ClassNotFoundException, SQLException {
        SkillFactory skillFactory = new SkillFactory();
        SkillDao skillDao = new SkillDao();
        int id;
        String name;
        String level;

        ConsoleHelper.writeMessage(SKILLS + ":" + "\n" +
                "1-Create | 2-Read All | 3-Read | 4-Update | 5-Delete | 0 - Exit\n");
        int commandNumber = ConsoleHelper.readInt();

        switch (commandNumber){
            case 1:
                ConsoleHelper.writeMessage("Enter name:\n");
                name = ConsoleHelper.readString();
                ConsoleHelper.writeMessage("Enter level:\n");
                level = ConsoleHelper.readString();
                ConsoleHelper.writeMessage("\n" + SKILL + " added!\n");
                skillFactory.createSkill(name, level);
                skillDao.showAllElements();
                break;
            case 2:
                skillDao.showAllElements();
                break;
            case 3:
                ConsoleHelper.writeMessage("Enter " + SKILL + " id:\n");
                id = ConsoleHelper.readInt();
                skillDao.showElement(id);
                break;
            case 4:
                ConsoleHelper.writeMessage("Enter " + SKILL + " id:\n");
                id = ConsoleHelper.readInt();
                ConsoleHelper.writeMessage("\nEnter new " + SKILL + " level:\n");
                level = ConsoleHelper.readString();
                skillDao.updateElement(id, level);
                ConsoleHelper.writeMessage("\n" + SKILL + " successfully updated!\n");
                break;
            case 5:
                ConsoleHelper.writeMessage("Enter " + SKILL + " id for element removal:\n");
                id = ConsoleHelper.readInt();
                skillDao.deleteElement(id);
                ConsoleHelper.writeMessage("\n" + SKILL + " successfully removed!\n");
                break;
            default:
                break;
        }
    }
}
