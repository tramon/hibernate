package ua.goit.hibernate.controller;

import ua.goit.hibernate.dao.CompanyDao;
import ua.goit.hibernate.factory.CompanyFactory;
import ua.goit.hibernate.view.ConsoleHelper;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by tramon on 30.11.2016.
 */
public class CompanyCommand implements Command{

    private final String COMPANY = "Company";
    private final String COMPANIES = "Companies";

    @Override
    public void execute() throws IOException, ClassNotFoundException, SQLException {
        CompanyFactory companyFactory = new CompanyFactory();
        CompanyDao companyDao = new CompanyDao();
        int id;
        String name;
        String address;

        ConsoleHelper.writeMessage(COMPANIES + ":" + "\n" +
                "1-Create | 2-Read All | 3-Read | 4-Update | 5-Delete | 0 - Exit\n");
        int commandNumber = ConsoleHelper.readInt();

        switch (commandNumber){
            case 1:
                ConsoleHelper.writeMessage("Enter name:\n");
                name = ConsoleHelper.readString();
                ConsoleHelper.writeMessage("Enter address:\n");
                address = ConsoleHelper.readString();
                ConsoleHelper.writeMessage("\n" + COMPANY + " added!\n");
                companyFactory.createCompany(name, address);
                companyDao.showAllElements();
                break;
            case 2:
                companyDao.showAllElements();
                break;
            case 3:
                ConsoleHelper.writeMessage("Enter " + COMPANY + " id:\n");
                id = ConsoleHelper.readInt();
                companyDao.showElement(id);
                break;
            case 4:
                ConsoleHelper.writeMessage("Enter " + COMPANY + " id:\n");
                id = ConsoleHelper.readInt();
                ConsoleHelper.writeMessage("\nEnter new " + COMPANY + " address:\n");
                address = ConsoleHelper.readString();
                companyDao.updateElement(id, address);
                ConsoleHelper.writeMessage("\n" + COMPANY + " successfully updated!\n");
                break;
            case 5:
                ConsoleHelper.writeMessage("Enter " + COMPANY + " id for removal:\n");
                id = ConsoleHelper.readInt();
                companyDao.deleteElement(id);
                ConsoleHelper.writeMessage("\n" + COMPANY + " successfully removed!\n");
                break;
            default:
                break;
        }
    }
}
