package ua.goit.hibernate.controller;

import ua.goit.hibernate.dao.DeveloperDao;
import ua.goit.hibernate.factory.DeveloperFactory;
import ua.goit.hibernate.model.Developer;
import ua.goit.hibernate.view.ConsoleHelper;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class DeveloperCommand implements Command {
    private final String DEVELOPER = "Developer";
    private final String DEVELOPERS = "Developers";

    @Override
    public void execute() throws IOException, ClassNotFoundException, SQLException {
        DeveloperFactory developerFactory = new DeveloperFactory();
        DeveloperDao developerDao = new DeveloperDao();

        int id;
        String name;
        String surname;
        int salary;

        ConsoleHelper.writeMessage(DEVELOPERS + ":" + "\n" +
                "1-Create | 2-Read All | 3-Read | 4-Update | 5-Delete | 0 - Exit\n");
        int commandNumber = ConsoleHelper.readInt();

        switch (commandNumber){
            case 1:
                ConsoleHelper.writeMessage("Enter name:\n");
                name = ConsoleHelper.readString();
                ConsoleHelper.writeMessage("Enter surname:\n");
                surname = ConsoleHelper.readString();
                ConsoleHelper.writeMessage("Enter "  + DEVELOPER + " salary:\n");
                salary = ConsoleHelper.readInt();
                developerFactory.createDeveloper(name, surname, salary);
                ConsoleHelper.writeMessage("\n" + DEVELOPER + " added!\n");
                developerDao.showAllElements();
                break;
            case 2:
                developerDao.showAllElements();
                break;
            case 3:
                ConsoleHelper.writeMessage("Enter " + DEVELOPER + " id:\n");
                id = ConsoleHelper.readInt();
                developerDao.showElement(id);
                break;
            case 4:
                ConsoleHelper.writeMessage("Enter " + DEVELOPER + " id:\n");
                id = ConsoleHelper.readInt();
                ConsoleHelper.writeMessage("Enter " + DEVELOPER + " salary:\n");
                salary = ConsoleHelper.readInt();
                developerDao.updateElement(id, salary);
                ConsoleHelper.writeMessage("\n" + DEVELOPER + " successfully updated!\n");
                break;
            case 5:
                ConsoleHelper.writeMessage("Enter " + DEVELOPER + " id for removal:\n");
                id = ConsoleHelper.readInt();
                developerDao.deleteElement(id);
                ConsoleHelper.writeMessage("\n" + DEVELOPER + " successfully removed!\n");
                break;
            default:
                break;
        }
    }
}
