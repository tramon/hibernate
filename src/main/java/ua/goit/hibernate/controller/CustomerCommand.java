package ua.goit.hibernate.controller;


import ua.goit.hibernate.dao.CustomerDao;
import ua.goit.hibernate.view.ConsoleHelper;
import ua.goit.hibernate.factory.CustomerFactory;

import java.io.IOException;
import java.sql.SQLException;


/**
 * Created by tramon on 30.11.2016.
 */
public class CustomerCommand implements Command {
    private final String CUSTOMER = "Customer";
    private final String CUSTOMERS = "Customers";

    @Override
    public void execute() throws IOException, ClassNotFoundException, SQLException {
        CustomerFactory customerFactory = new CustomerFactory();
        CustomerDao customerDao = new CustomerDao();
        int id;
        String name;

        ConsoleHelper.writeMessage(CUSTOMERS + ":" + "\n" +
                "1-Create | 2-Read All | 3-Read | 4-Update | 5-Delete | 0 - Exit\n");
        int commandNumber = ConsoleHelper.readInt();

        switch (commandNumber){
            case 1:
                ConsoleHelper.writeMessage("Enter name:\n");
                name = ConsoleHelper.readString();
                customerFactory.createCustomer(name);
                ConsoleHelper.writeMessage("\n" + CUSTOMER + " added!\n");
                customerDao.showAllElements();
                break;
            case 2:
                customerDao.showAllElements();
                break;
            case 3:
                ConsoleHelper.writeMessage("Enter " + CUSTOMER + " id:\n");
                id = ConsoleHelper.readInt();
                customerDao.showElement(id);
                break;
            case 4:
                ConsoleHelper.writeMessage("Enter " + CUSTOMER + " id:\n");
                id = ConsoleHelper.readInt();
                ConsoleHelper.writeMessage("\nEnter new " + CUSTOMER + " name:\n");
                name = ConsoleHelper.readString();
                customerDao.updateElement(id, name);
                ConsoleHelper.writeMessage("\n" + CUSTOMER + " successfully updated!\n");
                break;
            case 5:
                ConsoleHelper.writeMessage("Enter " + CUSTOMER + " id for removal:\n");
                id = ConsoleHelper.readInt();
                customerDao.deleteElement(id);
                ConsoleHelper.writeMessage("\n" + CUSTOMER + " successfully removed!\n");
                break;
            default:
                break;
        }
    }
}
