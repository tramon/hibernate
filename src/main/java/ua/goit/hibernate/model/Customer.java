package ua.goit.hibernate.model;


import org.hibernate.annotations.Type;

public class Customer {

    private int id;
    private String name;

    public Customer() {
    }
    
    
    public Customer(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "=====| Customer |======================================\n" +
                "id: \t\t\t" + id +  "\n" +
                "Name: \t\t\t" + name +  "\n";
    }
}
