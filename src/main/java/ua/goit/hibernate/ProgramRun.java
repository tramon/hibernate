package ua.goit.hibernate;

import ua.goit.hibernate.controller.*;
import ua.goit.hibernate.view.ConsoleHelper;

import java.io.IOException;
import java.sql.SQLException;

public class ProgramRun {

    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
        Command command = null;
        int commandNumber;
        ConsoleHelper.writeMessage("Hello this is the SQL JDBC CRUD App!");

        while (true) {
        ConsoleHelper.writeMessage("\nPlease choose: 1-Companies | 2-Developers | 3-Projects | 4-Customers | 0-Quit App");
            commandNumber = ConsoleHelper.readInt();
            switch (commandNumber) {
                case 0:
                    return;
                case 1:
                    command = new CompanyCommand();
                    command.execute();
                    break;
                case 2:
                    command = new DeveloperCommand();
                    command.execute();
                    break;
                case 3:
                    command = new ProjectCommand();
                    command.execute();
                    break;
                case 4:
                    command = new CustomerCommand();
                    command.execute();
                    break;
                case 5:
                    command = new SkillCommand();
                    command.execute();
                    break;
                default:
                    break;
            }

        }
    }
}
