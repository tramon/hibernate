package ua.goit.hibernate.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import ua.goit.hibernate.model.Project;

import java.util.List;

public class ProjectDao{
    private static SessionFactory sessionFactory;

    public ProjectDao() {
        sessionFactory = new Configuration().configure().buildSessionFactory();
    }

    public void createElement(String name, int cost) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Project project = new Project(name, cost);
        session.save(project);
        transaction.commit();
        session.close();
    }

    public List showAllElements() {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        List projects = session.createQuery("FROM Project").list();
        transaction.commit();
        printAll(projects);
        session.close();
        return projects;
    }

    public Project showElement(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Project project =  session.get(Project.class, id);
        transaction.commit();
        print(project);
        session.close();
        return project;
    }


    public void updateElement(int id, int cost) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Project project = session.get(Project.class, id);
        project.setCost(cost);
        session.update(project);
        transaction.commit();
        session.close();
    }

    public void deleteElement(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Project project = session.get(Project.class, id);
        session.delete(project);
        transaction.commit();
        session.close();
    }

    public void printAll(List<Project> list) {
        list.forEach(System.out::println);
    }

    public void print(Project project) {
        System.out.println(project);
    }
}
