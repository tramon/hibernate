package ua.goit.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import ua.goit.hibernate.model.Company;
import java.util.List;

/**
 * Created by tramon on 30.11.2016.
 */
public class CompanyDao {

    private static SessionFactory sessionFactory;

    public CompanyDao() {
        sessionFactory = new Configuration().configure().buildSessionFactory();
    }

    public void createElement(String name, String address) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Company company = new Company(name, address);
        session.save(company);
        transaction.commit();
        session.close();
    }

    public List showAllElements() {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        List companies = session.createQuery("FROM Company").list();
        transaction.commit();
        printAll(companies);
        session.close();
        return companies;
    }

    public Company showElement(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Company company =  session.get(Company.class, id);
        transaction.commit();
        print(company);
        session.close();
        return company;
    }


    public void updateElement(int id, String address) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Company company = session.get(Company.class, id);
        company.setName(address);
        session.update(company);
        transaction.commit();
        session.close();
    }

    public void deleteElement(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Company company = session.get(Company.class, id);
        session.delete(company);
        transaction.commit();
        session.close();
    }

    public void printAll(List<Company> list) {
        list.forEach(System.out::println);
    }

    public void print(Company company) {
        System.out.println(company);
    }

}
