package ua.goit.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import ua.goit.hibernate.model.Developer;

import java.util.List;

public class DeveloperDao{

    private static SessionFactory sessionFactory;

    public DeveloperDao() {
        sessionFactory = new Configuration().configure().buildSessionFactory();
    }

    public void createElement(String name, String surname, int salary) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Developer developer = new Developer(name, surname, salary);
        session.save(developer);
        transaction.commit();
        session.close();
    }

    public List showAllElements() {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        List developers = session.createQuery("FROM Developer").list();
        transaction.commit();
        printAll(developers);
        session.close();
        return developers;
    }

    public Developer showElement(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Developer developer =  session.get(Developer.class, id);
        transaction.commit();
        print(developer);
        session.close();
        return developer;
    }


    public void updateElement(int id, int salary) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Developer developer = session.get(Developer.class, id);
        developer.setSalary(salary);
        session.update(developer);
        transaction.commit();
        session.close();
    }

    public void deleteElement(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Developer developer = session.get(Developer.class, id);
        session.delete(developer);
        transaction.commit();
        session.close();
    }

    public void printAll(List<Developer> list) {
        list.forEach(System.out::println);
    }

    public void print(Developer developer) {
        System.out.println(developer);
    }

}
