package ua.goit.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import ua.goit.hibernate.model.Skill;

import java.util.List;

/**
 * Created by tramon on 30.11.2016.
 */
public class SkillDao {

    private static SessionFactory sessionFactory;

    public SkillDao() {
        sessionFactory = new Configuration().configure().buildSessionFactory();
    }

    public void createElement(String name, String level) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Skill skill = new Skill(name, level);
        session.save(skill);
        transaction.commit();
        session.close();
    }

    public List showAllElements() {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        List skills = session.createQuery("FROM Skill").list();
        transaction.commit();
        printAll(skills);
        session.close();
        return skills;
    }

    public Skill showElement(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Skill skill =  session.get(Skill.class, id);
        transaction.commit();
        print(skill);
        session.close();
        return skill;
    }


    public void updateElement(int id, String level) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Skill skill = session.get(Skill.class, id);
        skill.setName(level);
        session.update(skill);
        transaction.commit();
        session.close();
    }

    public void deleteElement(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Skill skill = session.get(Skill.class, id);
        session.delete(skill);
        transaction.commit();
        session.close();
    }

    public void printAll(List<Skill> list) {
        list.forEach(System.out::println);
    }

    public void print(Skill skill) {
        System.out.println(skill);
    }

}
