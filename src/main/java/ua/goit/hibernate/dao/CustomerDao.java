package ua.goit.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import ua.goit.hibernate.model.Customer;

import java.util.List;

/**
 * Created by tramon on 30.11.2016.
 */
public class CustomerDao {
    private static SessionFactory sessionFactory;

    public CustomerDao() {
        sessionFactory = new Configuration().configure().buildSessionFactory();
    }

    public void createElement(String name) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Customer customer = new Customer(name);
        session.save(customer);
        transaction.commit();
        session.close();
    }

    public List showAllElements() {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        List customers = session.createQuery("FROM Customer").list();
        transaction.commit();
        printAll(customers);
        session.close();
        return customers;
    }

    public Customer showElement(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Customer customer =  session.get(Customer.class, id);
        transaction.commit();
        print(customer);
        session.close();
        return customer;
    }


    public void updateElement(int id, String address) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Customer customer = session.get(Customer.class, id);
        customer.setName(address);
        session.update(customer);
        transaction.commit();
        session.close();
    }

    public void deleteElement(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;

        transaction = session.beginTransaction();
        Customer customer = session.get(Customer.class, id);
        session.delete(customer);
        transaction.commit();
        session.close();
    }

    public void printAll(List<Customer> list) {
        list.forEach(System.out::println);
    }

    public void print(Customer customer) {
        System.out.println(customer);
    }

}
